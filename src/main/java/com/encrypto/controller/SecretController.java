package com.encrypto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.encrypto.model.Secret;
import com.encrypto.service.EncryptionService;
import com.encrypto.service.SecretService;

@RestController
@RequestMapping("/v1")
public class SecretController {

	@Autowired
	private SecretService secretService;
	@Autowired
	private EncryptionService encryptionService;
	
	
	@GetMapping("/secrets")
	public List<Secret> getAllSecrets() {
		return encryptionService.decryptSecrets(secretService.getSecrets());
	}

	@GetMapping("/secretid")
	public Secret getSecret(@RequestParam Long id) {
		return encryptionService.decryptSecret(secretService.getSecret(id));
	}

	@GetMapping("/secretuserid")
	public Secret getSecretByUserId(@RequestParam String userId) {
		return encryptionService.decryptSecret(secretService.getSecretByUserid(userId));
	}
	
	@GetMapping("/encryptsecret")
	public List<Secret> getAllSecretsEncryptForm() {
		return secretService.getSecrets();
	}
	
	@PostMapping(value = "/savesecret")
	public Long saveSecret(@RequestBody Secret secret) {
		return secretService.saveSecret(secret);

	}
}
