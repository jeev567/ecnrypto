package com.encrypto.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.encrypto.model.Secret;

@Repository
public interface SecretRepository extends CrudRepository<Secret, Long> {

	Secret findByUserId(String userid);
}
