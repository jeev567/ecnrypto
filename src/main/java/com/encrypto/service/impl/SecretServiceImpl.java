package com.encrypto.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.encrypto.model.Secret;
import com.encrypto.repository.SecretRepository;
import com.encrypto.service.EncryptionService;
import com.encrypto.service.SecretService;

@Service
public class SecretServiceImpl implements SecretService {

	@Autowired
	private SecretRepository secretRepository;
	
	@Autowired
	private EncryptionService encryptionService;
	@Override
	public Long saveSecret(Secret secret) {
		
		secret.setPassword(encryptionService.encrypt(secret.getPassword()));
		secret.setUrl(encryptionService.encrypt(secret.getUrl()));
		
		secret = secretRepository.save(secret);
		if (secret != null) {
			return secret.getId();
		} else {
			return -1L;
		}
	}

	@Override
	public List<Secret> getSecrets() {
		return (List<Secret>) secretRepository.findAll();
	}

	@Override
	public Secret getSecret(Long id) {
		Optional<Secret> output = secretRepository.findById(id);
		return output.get();
	}

	@Override
	public Secret getSecretByUserid(String userid) {
		return secretRepository.findByUserId(userid);
	}
}
