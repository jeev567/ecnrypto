package com.encrypto.service.impl;

import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.encrypto.model.Secret;
import com.encrypto.service.EncryptionService;

@Service
public class EncryptionServiceImpl implements EncryptionService {

	@Value("${encryption.key}")
	private String secret;

	@Value("${encryption.salt}")
	private String salt;

	@Override
	public String encrypt(String strToEncrypt) {
		try {
			final byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			final IvParameterSpec ivspec = new IvParameterSpec(iv);

			final SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
			final KeySpec spec = new PBEKeySpec(secret.toCharArray(), salt.getBytes(), 65536, 256);
			final SecretKey tmp = factory.generateSecret(spec);
			final SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");

			final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivspec);
			return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
		} catch (final Exception e) {
			System.out.println("Error while encrypting: " + e.toString());
		}
		return null;
	}

	@Override
	public String decrypt(String strToDecrypt) {
		try {
			final byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			final IvParameterSpec ivspec = new IvParameterSpec(iv);

			final SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
			final KeySpec spec = new PBEKeySpec(secret.toCharArray(), salt.getBytes(), 65536, 256);
			final SecretKey tmp = factory.generateSecret(spec);
			final SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");

			final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, secretKey, ivspec);
			return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
		} catch (final Exception e) {
			System.out.println("Error while decrypting: " + e.toString());

		}
		return "Wrong Decryption";
	}

	@Override
	public List<Secret> decryptSecrets(List<Secret> secrets) {
		for (Secret secret : secrets) {
			secret.setUrl(decrypt(secret.getUrl()));
			secret.setPassword(decrypt(secret.getPassword()));
		}
		return secrets;
	}

	@Override
	public Secret decryptSecret(Secret secret) {
		secret.setUrl(decrypt(secret.getUrl()));
		secret.setPassword(decrypt(secret.getPassword()));
		return secret;

	}

}
