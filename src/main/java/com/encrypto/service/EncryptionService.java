package com.encrypto.service;

import com.encrypto.model.Secret;
import java.util.List;

public interface EncryptionService {

	public String encrypt(String strToEncrypt);

	public String decrypt(String strToDecrypt);

	public List<Secret> decryptSecrets(List<Secret> secrets);

	public Secret decryptSecret(Secret secret);

}
