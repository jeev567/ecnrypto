package com.encrypto.service;

import java.util.List;

import com.encrypto.model.Secret;

public interface SecretService {

	public Long saveSecret(Secret secret);

	public List<Secret> getSecrets();

	public Secret getSecret(Long id);

	public Secret getSecretByUserid(String userid);

}
